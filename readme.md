### Run test project
 - `composer install`
 - `cp .env.example .env`
 - modify DB connection params on `.env`
 - `php artisan migrate --seed`

### Get access token
POST http://127.0.0.1/laravel-passport-test/public/oauth/token
```
[POST body]
grant_type:password
client_id:1
client_secret:[check this on your oauth_clients table]
username:test@example.com
password:12345
```

### Test your oauth api
GET http://127.0.0.1/laravel-passport-test/public/api/user
```
[Headers]  
Accept:application/json  
Authorization:Bearer [your access token]  
```
if response success, then you will get : 
```json
{
    "id": 1,
    "name": "test",
    "email": "test@example.com",
    "created_at": "2018-08-11 13:56:28",
    "updated_at": "2018-08-11 13:56:28"
}
```

