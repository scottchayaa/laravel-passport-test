<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class oauth_clients extends Model
{
    public $timestamps = true;
    protected $table   = 'oauth_clients';

}
